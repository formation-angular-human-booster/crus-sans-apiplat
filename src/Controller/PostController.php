<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/posts', name: 'post_', format: 'json')]
class PostController extends AbstractController
{
    private $postRepository;
    private $serializer;
    private $headers = ['Content-Type'=> 'application/json'];

    public function __construct(PostRepository $postRepository, SerializerInterface $serializer){
        $this->postRepository = $postRepository;
        $this->serializer = $serializer;
    }

    #[Route('/', name: 'getall', methods: ['GET'])]
    public function getAll(): Response
    {
        $posts = $this->postRepository->findAll();
        return new Response($this->serializer->serialize($posts, 'json'),
            Response::HTTP_OK, $this->headers);
    }

    #[Route('/{post}', name: 'get_one', methods: ['GET'])]
    public function getOne(Post $post): Response
    {
        return new Response($this->serializer->serialize($post, 'json'),
            Response::HTTP_OK, $this->headers);
    }

    #[Route('/', name: 'add', methods: ['POST'])]
    public function post(Request $request, EntityManagerInterface $entityManager)
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(PostType::class, new Post());
        $form->submit($data);
        if($form->isValid()){
            $post = $form->getData();
            $entityManager->persist($post);
            $entityManager->flush();
            return new Response($this->serializer->serialize($post, 'json'),
                Response::HTTP_CREATED, $this->headers);
        } else {
            return new Response($this->serializer->serialize($form->getErrors(), 'json'),
                Response::HTTP_BAD_REQUEST,
                $this->headers);
        }
    }

    #[Route('/{post}', name: 'update', methods: ['PUT'])]
    public function put(Post $post, Request $request, EntityManagerInterface $entityManager)
    {
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(PostType::class, $post);
        $form->submit($data);
        if($form->isValid()){
            $post = $form->getData();
            $entityManager->flush();
            return new Response($this->serializer->serialize($post, 'json'),
                Response::HTTP_CREATED, $this->headers);
        } else {
            return new Response($this->serializer->serialize($form->getErrors(), 'json'),
                Response::HTTP_BAD_REQUEST,
                $this->headers);
        }
    }

    #[Route('/{post}', name: 'delete', methods: ['DELETE'])]
    public function delete(Post $post, EntityManagerInterface $em){
        $em->remove($post);
        $em->flush();
        return new JsonResponse(['success'=> true]);
    }

}
